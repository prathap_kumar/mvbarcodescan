import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:mvbarcodescan/mvbarcodescan.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String? _scannedBarCode = '';

  @override
  void initState() {
    super.initState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarCode() async {
    String? barCode;
    try {
      barCode = await Mvbarcodescan.scan;
    } on PlatformException {
      barCode = '';
    }

    setState(() {
      _scannedBarCode = barCode!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            scanBarCode();
          },
          child: Icon(Icons.camera),
        ),
        appBar: AppBar(
          title: const Text('Barcode Scan'),
        ),
        body: Center(
          child: Text('Scanned Barcode : ' + _scannedBarCode!),
        ),
      ),
    );
  }
}
