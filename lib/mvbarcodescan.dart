import 'dart:async';

import 'package:flutter/services.dart';

class Mvbarcodescan {
  static const MethodChannel _channel =
      const MethodChannel('mvbarcodescan');

  static Future<String> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version!;
  }


  static Future<String> get scan async {
    final String? barcode = await _channel.invokeMethod('scan');
    return barcode!;
  }
}
