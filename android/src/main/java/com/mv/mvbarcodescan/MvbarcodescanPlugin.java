package com.mv.mvbarcodescan;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * MvbarcodescanPlugin
 */
public class MvbarcodescanPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, PluginRegistry.ActivityResultListener {

    private static final String channelName = "mvbarcodescan";

    private static final int REQUEST_CODE_FOR_QR_CODE_SCAN = 2999;


    private MethodChannel channel;
    private static Activity activity;
    private Result pendingResult;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), channelName);
        channel.setMethodCallHandler(this);
    }


    public static void registerWith(Registrar registrar) {
        activity = registrar.activity();
        final MethodChannel channel = new MethodChannel(registrar.messenger(), channelName);
        channel.setMethodCallHandler(new MvbarcodescanPlugin());
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("scan")) {

            this.pendingResult = result;


            Intent intent = new Intent(activity, QrCodeScanActivity.class);

            activity.startActivityForResult(intent, REQUEST_CODE_FOR_QR_CODE_SCAN);


        } else {
            result.notImplemented();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding activityPluginBinding) {

        activity = activityPluginBinding.getActivity();

        activityPluginBinding.addActivityResultListener(this);

    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding activityPluginBinding) {

    }

    @Override
    public void onDetachedFromActivity() {

        activity = null;

    }


    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == REQUEST_CODE_FOR_QR_CODE_SCAN && data != null) {
            if (resultCode == Activity.RESULT_OK) {

                String qrCodeData = data.getStringExtra("BARCODE");
                pendingResult.success(qrCodeData);

            } else {

                pendingResult.success("");
            }

            return true;
        }

        return false;

    }

}
