package com.mv.mvbarcodescan;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture;
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever;

public class QrCodeScanActivity extends AppCompatActivity implements BarcodeRetriever {


    // vision barcode
    BarcodeCapture barcodeCapture;

    ImageButton btn_showTorch;

    boolean showTorch = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcodescan);
        btn_showTorch = (ImageButton) findViewById(R.id.btn_showTorch);
        barcodeCapture = (BarcodeCapture) getSupportFragmentManager().findFragmentById(R.id.cameraPreview);
        barcodeCapture.setRetrieval(this);
        barcodeCapture.setShowDrawRect(true);
       // barcodeCapture.shouldAutoFocus(true);


      // barcodeCapture.setShowFlash(showTorch);


        btn_showTorch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (showTorch==true) {
                    showTorch = false;
                    barcodeCapture.setShowFlash(showTorch);


                } else {

                    showTorch = true;
                    barcodeCapture.setShowFlash(showTorch);

                }
                barcodeCapture.refresh(true);


            }
        });

    }


    @Override
    public void onRetrieved(final Barcode barcode) {


        //Log.d(TAG, "Barcode read: " + barcode.displayValue);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                final String barcode1 = barcode.displayValue;
                Log.i("Detected BarCode", barcode1);

                if (barcode1.length() != 0) {

                    // pause barcode scanning to show AlertBox
                    barcodeCapture.pause();
                    AlertDialog.Builder builder = new AlertDialog.Builder(QrCodeScanActivity.this);
                    builder.setTitle("Confirm Barcode");
                    builder.setMessage(barcode1);
                    builder.setCancelable(false);
                    builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("BARCODE", barcode1);
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            barcodeCapture.resume();
                            barcodeCapture.refresh(true);
                        }
                    });
                    builder.show();


                } else {
                    showMessage("No barcode found");
                }


            }
        });


    }


    public void showMessage(String message) {
        if (message == null || message.length() == 0)
            return;

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRetrievedMultiple(Barcode closetToClick, List<BarcodeGraphic> barcode) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onRetrievedFailed(String reason) {

    }

    @Override
    public void onPermissionRequestDenied() {


    }
}
