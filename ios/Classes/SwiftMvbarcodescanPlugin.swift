import Flutter
import UIKit

public class SwiftMvbarcodescanPlugin: NSObject, FlutterPlugin,BarcodeScannerDelegate {
    
    var barcode: String = ""
     private var pendingResult: FlutterResult?
     private var hostViewController: UIViewController?

    
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "mvbarcodescan", binaryMessenger: registrar.messenger())
    let instance = SwiftMvbarcodescanPlugin()
    instance.hostViewController = UIApplication.shared.delegate?.window??.rootViewController

    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    

  self.pendingResult = result
        if ("scan" == call.method) {

            if #available(iOS 10.0, *) {
                showBarcodeView()
            } else {
                // Fallback on earlier versions
            }

            }
         else {
            result("not implemented")
        }

    


  }

    @available(iOS 10.0, *)
    private func showBarcodeView() {
          let scannerViewController = AVScanViewController()

          let navigationController = UINavigationController(rootViewController: scannerViewController)

            scannerViewController.delegate = self

          if #available(iOS 13.0, *) {
              navigationController.modalPresentationStyle = .fullScreen
          }


          hostViewController?.present(navigationController, animated: false)
      }



 func didScanBarcodeWithResult(data: String) {
    
    barcode = data;

    pendingResult?(data)

    
    
    }


}






