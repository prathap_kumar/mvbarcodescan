#import "MvbarcodescanPlugin.h"
#if __has_include(<mvbarcodescan/mvbarcodescan-Swift.h>)
#import <mvbarcodescan/mvbarcodescan-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "mvbarcodescan-Swift.h"
#endif

@implementation MvbarcodescanPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMvbarcodescanPlugin registerWithRegistrar:registrar];
}
@end
